import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Release extends Component {
  state = {
    selected: false
  }

  render() {
    const { artists, labels, formats, cover_image, title, year } = this.props.data.basic_information
    return (
      <div style={styles.release}>
        <Link to={`/release/${this.props.data.basic_information.id}`}>
          <img src={cover_image} style={styles.image} />
        </Link>
        <div style={styles.releaseInner}>
          <h2 style={styles.header}>
            {title} ({formats.map(format => format.name).join(', ')})
          </h2>

          <div style={styles.releaseDetails}>
            <div>
              {artists.map(artist => `${artist.name}${artist.join == ',' ? '' : ' '}${artist.join}`).join(' ')}
            </div>
            <div>
              {labels.map(label => `${label.catno} ${label.name}`).join(', ')}
            </div>
            <div>
              {year}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const styles = {
  release: {
    margin: '.5em',
    overflow: 'hidden',
    background: '#fff',
    color: '#666',
    width: '18%',
  },
  releaseInner: {
    padding: '.8em',
  },
  releaseDetails: {
    fontSize: '85%',
  },
  image: {
    width: '100%',
  },
  header: {
    margin: '0 0 .3em 0',
    fontFamily: 'Roboto 300',
    fontWeight: 'normal',
    fontSize: '125%',
    color: '#444',
  },
}
