import React, { Component } from 'react'

export default class SearchResults extends Component {
  renderRelease = (release, i) => {
    return (
      <div key={release.id} style={styles.result}>
        <img src={ release.thumb } width={50} />
        { release.title }
      </div>
    )
  }

  render() {
    return (
      <div style={styles.results}>
        { this.props.releases.map(this.renderRelease) }
      </div>
    )
  }
}

const styles = {
  results: {
    background: 'white',
    maxHeight: '300px',
    position: 'absolute',
    left: '131px',
    overflowY: 'scroll',
  },
  result: {
    padding: '.5em',
  }
}

