import React, { Component } from 'react'
import Header from './Header'
import APIRequest from './lib/API'
import Artist from './release/Artist'
import Label from './release/Label'
import Tracklist from './release/Tracklist'
import Format from './release/Format'
import Credits from './release/Credits'
import Labels from './release/Labels'
import Notes from './release/Notes'
import Formats from './release/Formats'

export default class ReleaseDetail extends Component {
  state = {
    release: {
      artists: [],
      companies: [],
      extraartists: [],
      formats: [],
      genres: [],
      identifiers: [],
      images: [],
      labels: [],
      tracklist: [],
    },
  }

  componentWillMount() {
    const releaseId = this.props.match.params.release_id
    APIRequest(`https://api.discogs.com/releases/${releaseId}`)
      .then(body => {
        this.setState({
          release: body
        })
      })
  }

  render() {
    const {
      title,
      thumb,
      year,
      released,
      country,
      notes,
      artists,
      labels,
      tracklist,
      formats,
      extraartists,
    } = this.state.release

    return (
      <div>
        <Header />
        <div style={styles}>
          <img src={ thumb } />
          <h1 style={styles.title}>{ title }</h1>
          <h2 style={styles.artists}>
            {artists.map(artist => <Artist key={artist.id} data={artist} /> )}
          </h2>

          <p>Released { released } in { country }</p>

          <Formats data={ formats } />

          <Notes data={ notes } />

          <Labels data={ labels } />

          <Tracklist data={ tracklist } />

          <Credits data={ extraartists } />
        </div>
      </div>
    )
  }
}

const styles = {
  padding: '1em',
  title: {
    margin: 0,
    padding: 0,
  },
  artists: {
    margin: 0,
    padding: 0,
  },
}
