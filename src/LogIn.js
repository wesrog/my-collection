import React, { Component } from 'react'
import Button from 'material-ui/Button'

const rp = require('request-promise')
const ls = window.localStorage
const qs = require('querystring')
import {OAUTH_TOKEN, OAUTH_TOKEN_SECRET, CONSUMER_KEY, CONSUMER_SECRET, API_HEADERS} from './config'

export default class LogIn extends Component {
  requestToken() {
    let oauth = {
      callback: window.location.href,
      consumer_key: CONSUMER_KEY,
      consumer_secret: CONSUMER_SECRET,
      signature_method: 'PLAINTEXT',
      transport_method: 'body',
    }

    let url = 'https://api.discogs.com/oauth/request_token'

    rp.post({url: url, oauth: oauth, headers: API_HEADERS })
      .then((body) => {
        let req_data = qs.parse(body)
        ls.setItem('oauth_token', req_data.oauth_token_secret)
        let url = `https://www.discogs.com/oauth/authorize?${qs.stringify({oauth_token: req_data.oauth_token})}`
        window.location = url
      })
  }

  accessToken() {
    let qs_data = qs.parse(window.location.href.split('?')[1])
    let oauth = {
      consumer_key: CONSUMER_KEY,
      consumer_secret: CONSUMER_SECRET,
      signature_method: 'PLAINTEXT',
      transport_method: 'body',
      verifier: qs_data.oauth_verifier,
      token: qs_data.oauth_token,
      token_secret: ls.getItem('oauth_token'),
    }

    let url = 'https://api.discogs.com/oauth/access_token'

    rp.post({url: url, oauth: oauth, headers: API_HEADERS })
      .then((body) => {
        ls.removeItem('oauth_token')
        qs_data = qs.parse(body)
        ls.setItem('access_token', qs_data.oauth_token)
        ls.setItem('access_token_secret', qs_data.oauth_token_secret)

        let url = 'https://api.discogs.com/oauth/identity'
        let oauth = {
          consumer_key: CONSUMER_KEY,
          consumer_secret: CONSUMER_SECRET,
          token: qs_data.oauth_token,
          token_secret: qs_data.oauth_token_secret,
        }

        let options = {
          url: url,
          oauth: oauth,
          headers: API_HEADERS,
          json: true,
        }

        rp.get(options)
          .then((body) => {
            ls.setItem('username', body.username)
            window.location = window.location.origin
          })
      })
  }

  componentDidMount() {
    if(window.location.href.includes('oauth_token')) {
      this.accessToken()
    }
  }

  render() {
    return (
      <div style={{textAlign: 'center'}}>
        <Button variant="raised" color="primary" onClick={this.requestToken}>Log In</Button>
      </div>
    )
  }
}
