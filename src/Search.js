import React, { Component } from 'react'
import APIRequest from './lib/API'
import SearchResults from './SearchResults'

export default class Search extends Component {
  state = {
    results: [],
    showResults: false,
  }

  hideSearch = (e) => {
    this.setState({
      showResults: false,
    })
  }

  toggleSearch = (e) => {
    console.log
    this.setState({
      showResults: e.value == '' ? false : true
    })
  }

  search = (e) => {
    const query = e.target.value

    if (e.charCode !== 13) {
      return
    }

    let url = `https://api.discogs.com/database/search?q=${query}`
    APIRequest(url)
      .then(body => {
        console.log(body.results)
        if (body.results) {
          this.setState({
            results: body.results,
            showResults: true,
          })
        }
      })
    e.preventDefault()
  }

  render() {
    return (
      <div style={styles}>
        Search Discogs:
        <input type="text"
          onKeyPress={this.search}
          onBlur={this.hideSearch}
          onFocus={this.toggleSearch}
        />

        {this.state.showResults ? (<SearchResults releases={this.state.results} />) : null }
      </div>
    )
  }
}

const styles = {
  flex: '3',
}
