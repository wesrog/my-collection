import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import CssBaseline from 'material-ui/CssBaseline'

import LogIn from './LogIn'
import Collection from './Collection'
import ReleaseDetail from './ReleaseDetail'
import Artist from './Artist'

export default class App extends Component {
  render() {
    return (
      <div>
        <CssBaseline />
        <Route path="/login" component={LogIn} />
        <Route path="/release/:release_id" component={ReleaseDetail} />
        <Route exact path="/" component={Collection} />
      </div>
    )
  }
}
