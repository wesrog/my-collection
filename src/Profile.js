import React, { Component } from 'react'
import Button from 'material-ui/Button'

const ls = window.localStorage

export default class Profile extends Component {
  logout() {
    ls.removeItem('access_token')
    this.props.history.push('/login')
  }

  render() {
    const { username, avatar_url } = this.props.data

    return (
      <div style={styles}>
        Hello, {username}!
        <img src={avatar_url} width={25} />
        <Button onClick={this.logout}>Logout</Button>
      </div>
    )
  }
}

const styles = {
  flex: '1',
}

