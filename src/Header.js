import React, { Component } from 'react'
import APIRequest from './lib/API'
import Search from './Search'
import Profile from './Profile'

const ls = window.localStorage

export default class App extends Component {
  state = {
    userInfo: JSON.parse(ls.getItem('userInfo')) || {}
  }

  componentWillMount() {
    if (ls.getItem('access_token')) {
      this.getUserInfo()
    }
  }

  getUserInfo() {
    if (ls.getItem('userInfo')) {
      return;
    }
    APIRequest(`https://api.discogs.com/users/${ls.getItem('username')}`)
      .then(body => {
        this.setState({userInfo: body })
        ls.setItem('userInfo', JSON.stringify(body))
      })
  }

  render() {
    return (
      <div style={styles}>
        <Search />
        <Profile data={this.state.userInfo} />
      </div>
    )
  }

}

const styles = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  background: '#666',
  padding: '.5em',
}
