import React, { Component } from 'react'

export default class Label extends Component {

  render() {
    const { name, catno } = this.props.data

    return `${name} (${catno})`
  }

}
