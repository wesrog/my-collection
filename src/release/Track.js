import React, { Component } from 'react'

export default class Track extends Component {

  render() {
    const {
      duration,
      position,
      title,
    } = this.props.data

    return (
      <li>
        {position} - {title} {(duration === '') ? '' : ' - ' + duration}
      </li>
    )
  }

}

