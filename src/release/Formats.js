import React, { Component } from 'react'
import Format from './Format'

export default class Formats extends Component {

  render() {
    return (
      <div>
        <h4>Formats</h4>
        <ul>
          {this.props.data.map(format => <li key={format.id} ><Format data={format} /></li>) }
        </ul>
      </div>
    )
  }

}
