import React, { Component } from 'react'

export default class Format extends Component {

  render() {
    const { name, qty, descriptions } = this.props.data

    return (
      `${name} (${descriptions.join(', ')})`
    )
  }

}
