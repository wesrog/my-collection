import React, { Component } from 'react'
import Label from './Label'

export default class Labels extends Component {

  render() {
    console.log(this.props.data)
    return (
      <div>
        <h4>Labels</h4>
        <ul>
          { this.props.data.map(label => <li key={label.id}><Label data={label} /></li>)}
        </ul>
      </div>
    )
  }

}
