import React, { Component } from 'react'
import Track from './Track'

export default class Tracklist extends Component {

  render() {
    const { tracks } = this.props.data

    return (
      <div>
        <h4>Tracks</h4>
        <ul>
          { this.props.data.map((track, i) => <Track key={i} data={track} />) }
        </ul>
      </div>
    )
  }

}

