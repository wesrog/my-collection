import React, { Component } from 'react'
import Artist from './Artist'

export default class Credits extends Component {

  render() {
    return (
      <div>
        <h4>Credits</h4>
        <ul>
          { this.props.data.map(credit => <li key={credit.id}><Artist data={credit} /></li>) }
        </ul>
      </div>
    )
  }

}
