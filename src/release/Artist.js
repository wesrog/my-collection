import React, { Component } from 'react'

export default class Artist extends Component {

  render() {
    const { name, join, role } = this.props.data

    return `${name} ${(join === ',' ? '' : ' ') + join}${(role === '' ? '' : ' (' + role + ')')}`
  }

}
