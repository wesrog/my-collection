import React, { Component } from 'react'

export default class Notes extends Component {

  render() {
    let notes = this.props.data

    return (
      <div>
        <h4>Notes</h4>
        <p>{ notes }</p>
      </div>
    )
  }

}
