const rp = require('request-promise')
import {OAUTH_TOKEN, OAUTH_TOKEN_SECRET, CONSUMER_KEY, CONSUMER_SECRET, API_HEADERS} from '../config'

export default function APIRequest(url, qs={}) {
  let oauth = {
    consumer_key: CONSUMER_KEY,
    consumer_secret: CONSUMER_SECRET,
    token: OAUTH_TOKEN,
    token_secret: OAUTH_TOKEN_SECRET,
  }

  let options = {
    url: url,
    oauth: oauth,
    headers: API_HEADERS,
    json: true,
    qs: qs
  }

  console.log(url)

  return rp.get(options)
}
