export const OAUTH_TOKEN = window.localStorage.getItem('access_token')
export const OAUTH_TOKEN_SECRET = window.localStorage.getItem('access_token_secret')
export const CONSUMER_KEY = process.env.REACT_APP_DISCOGS_CONSUMER_KEY
export const CONSUMER_SECRET = process.env.REACT_APP_DISCOGS_CONSUMER_SECRET

export const API_HEADERS = {
  'User-Agent': 'mycollection v0.999',
  'Content-Type': 'application/x-www-form-urlencoded',
}
