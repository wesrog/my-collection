import React, { Component } from 'react'

import Release from './collection/Release'
import APIRequest from './lib/API'
import Header from './Header'

const ls = window.localStorage

const isSearched = searchTerm =>
  release =>
    [
      release.basic_information.title.toLowerCase(),
      release.basic_information.artists.map(artist => artist.name).join(' '),
      release.basic_information.labels.map(label => label.name).join(' '),
    ].join(' ').toLowerCase().includes(searchTerm.toLowerCase())

export default class Collection extends Component {
  constructor(props) {
    super(props)
    this.onSearchChange = this.onSearchChange.bind(this)
  }

  state = {
    collection: [],
    loading: false,
    loadingPage: 1,
    searchTerm: '',
  }

  renderRelease = release => {
    return (
      <Release data={release} />
    )
  }

  recurseCollection(page=1, sort='added', sortOrder='desc') {
    this.setState({loading: true})
    const cache = ls.getItem('collection')
    if (cache) {
      this.setState({
        collection: JSON.parse(cache),
        loading: false,
      })
      return JSON.parse(cache).releases
    }
    APIRequest(`https://api.discogs.com/users/${ls.getItem('username')}/collection/folders/0/releases`, {per_page: 500, page: page, sort: sort, sort_order: sortOrder})
      .then(body => {
        this.setState({
          collection: this.state.collection.concat(body.releases)
        })
        if (page < body.pagination.pages) {
          //setTimeout(this.recurseCollection(page + 1), 1000)
          this.setState({
            loadingPage: (page + 1)
          })
        }
        ls.setItem('collection', JSON.stringify(body))
        return body
      })
      .then(body => {
        this.setState({
          loading: false,
        })
      })
  }

  componentWillMount() {
    if (ls.getItem('access_token')) {
      this.recurseCollection(this.state.loadingPage)
    } else {
      this.props.history.push('/login')
    }
  }

  onSearchChange(event) {
    this.setState({
      searchTerm: event.target.value.trim()
    })
  }

  render() {
    let output

    if (this.state.loading) {
      output = (
        <div>
          <Header />
          <h1>Loading page {this.state.loadingPage}&hellip;</h1>
        </div>
      )
    } else {
      const releases = this.state.collection.releases
      output = (
        <div>
          <Header />
          Search Collection: <input type="text" onChange={this.onSearchChange} />
          <div style={styles.coverViewMode}>
            {
              releases.filter(isSearched(this.state.searchTerm)).map(release =>
                <Release
                  key={release.instance_id}
                  data={release}
                />
              )
            }
          </div>
        </div>
      )
    }

    return output
  }
}

const styles = {
  coverViewMode: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
  }
}
