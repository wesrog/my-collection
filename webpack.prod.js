const path = require('path')
const Dotenv = require('dotenv-webpack')

module.exports = options => {
  return {
    entry: './src/index.js',
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist'),
      publicPath: '/',
    },
    node: {
      fs: 'empty',
      tls: 'empty',
      net: 'empty',
    },
    plugins: [
      new Dotenv({
        path: './.env',
        systemvars: true,
      }),
    ],
    module: {
      rules: [
        {
          test: /.js$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                cacheDirectory: true,
              },
            },
          ],
        },
      ],
    },
  }
}
